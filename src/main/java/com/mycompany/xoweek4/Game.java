/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.xoweek4;

import java.util.Scanner;

/**
 *
 * @author bbnpo
 */
public class Game {

    private Player player1;
    private Player player2;
    private Table table;

    public Game() {
        this.player1 = new Player('O');
        this.player2 = new Player('X');
    }

    public void play() {
        showWelcome();
        newGame();
        while (true) {
            showTable();
            showTurn();
            inputRowCol();
            if (table.checkWin()) {
                showTable();
                printwin();
                showInfo();
                Continue();

            }
            if (table.checkDraw()) {
                showTable();
                printdraw();
                showInfo();
                Continue();

            }

            table.switchPlayer();
        }

    }

    public void newGame() {
        this.table = new Table(player1, player2);

    }

    private void showWelcome() {
        System.out.println("Welcome to OX!");
    }

    private void showTable() {
        char[][] t = table.getTable();
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                System.out.print(t[i][j] + " ");
            }
            System.out.println(" ");

        }

    }

    private void showTurn() {
        System.out.println(table.getCurrentPlayer().getSymbol() + " turn");
    }

    private void inputRowCol() {
    Scanner sc = new Scanner(System.in);
    while (true) {
        System.out.println("Please input row,col (row [1-3] and column [1-3])");
        int row = sc.nextInt();
        int col = sc.nextInt();

      
        if (table.getTable()[row - 1][col - 1] == '-') {
            table.setRowCol(row, col);
            return;
        } else {
            System.out.println("Position is already taken. Please choose another position.");
        }
    }
}


    private void printwin() {

        System.out.println(table.getCurrentPlayer().getSymbol() + " wins!!");
    }

    private void printdraw() {
        System.out.println(table.getCurrentPlayer().getSymbol() + " draw!!");
    }

    private void Continue() {
        Scanner sc = new Scanner(System.in);
        System.out.print("Continue (y/n): ");
        String playAgain = sc.next();

        if (playAgain.equalsIgnoreCase("y")) {
            newGame();
        } else if (playAgain.equalsIgnoreCase("n")) {
            System.out.println("Goodbye!");
            System.exit(0);

        }
        table.switchPlayer();

    }

    private void showInfo() {
         System.out.println(player1);
         System.out.println(player2);
                
                
    }
    
}
