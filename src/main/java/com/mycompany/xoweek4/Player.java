/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.xoweek4;

/**
 *
 * @author bbnpo
 */
public class Player {

  private char symbol;
  private int winCount,loseCount,drawCount; 

    public char getSymbol() {
        return symbol;
    }

    
    public Player(char symbol) {
        this.symbol = symbol;
    }
    
    public void win (){
        winCount++;
        
    }
    public void lose (){
        loseCount++;
        
    }
    public void draw (){
        drawCount++;
        
    }

    public int getWinCount() {
        return winCount;
    }

    public int getLoseCount() {
        return loseCount;
    }

    public int getDrawCount() {
        return drawCount;
    }

    @Override
    public String toString() {
        return "Player{" + "Player=" + symbol + ", win=" + winCount + ", lose=" + loseCount + ", draw=" + drawCount + '}';
    }
    
    
}
